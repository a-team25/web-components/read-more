# Read More - Web Component

[Stephen Scaff](https://github.com/stephenscaff) | [Github](https://github.com/stephenscaff/read-smore) | [Doku](https://stephenscaff.github.io/read-smore/)

A customizable, lightweight vanilla JS plugin for truncating content with a Read more / Read less move, whilst preserving the original markup. Able to truncate by max word or character count.

<hr>

[[_TOC_]]

## Installation

### Npm
```bash
echo @a-team25:registry=https://gitlab.com/api/v4/projects/47253489/packages/npm/ >> .npmrc
```

```bash
npm i @a-team25/at-read-more
```

### Yarn
```bash
echo \"@a-team25:registry\" \"https://gitlab.com/api/v4/projects/47253489/packages/npm/\" >> .yarnrc
```

```bash
yarn add @a-team25/at-read-more
```

## Usage

### Import
Die Komponente muss nur in der "Haupt" JavaScript-Datei des Projekts importiert werden. <br>
Nach dem Import steht die Komponente **global** im Projekt zur Verfügung und es muss nichts weiter getan werden.

```javascript
import '@a-team25/at-read-more'
```

### Attributes

All parameters can be passed as attributes

```html
<at-read-more
    chars-count="17"
    more-text="Read more"
    less-text="Read less"
    ... 
>
    <p>Stuff and words and stuff and words.</p>
    <p>Words and stuff and words and stuff.</p>
</at-read-more>

```
| Name               | Default        | Description                                             |
|:-------------------|:---------------|:--------------------------------------------------------|
| `block-class-name` | `'read-smore'` | block class name                                        |
| `more-text`        | `'Read more'`  | defines the "uncollapse button text                     |
| `less-tex`         | `'Read less'`  | defines the "collapse button text                       |
| `words-count`      | `70`           | max word count in collapsed state                       |
| `chars-count`      | `null`         | max char count in collapsed state                       |
| `is-inline`        | `false`        | allignes the un-/collpase button at the end of the text |


### Template

```html
<at-read-more
    chars-count="60"
    more-text="Read more"
    less-text="Read less"
>
    <p>Stuff and words and stuff and words.</p>
    <p>Words and stuff and words and stuff.</p>
</at-read-more>
```

## License

[MIT](http://opensource.org/licenses/MIT)
