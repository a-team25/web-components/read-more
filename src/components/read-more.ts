import ReadSmore from 'read-smore'
import {LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';

export type Options = {
    blockClassName: string,
    moreText: string,
    lessText: string,
    wordsCount: number,
    charsCount: number|null,
    isInline: boolean,
};

@customElement('adt-read-more')
export class AdtReadMore extends LitElement {

    @property({attribute: 'block-class-name', reflect: false, type: String})
    public blockClassName = 'read-smore';

    @property({attribute: 'more-text', reflect: false, type: String})
    public moreText = 'Read more';

    @property({attribute: 'less-text', reflect: false, type: String})
    public lessText = 'Read less';

    @property({attribute: 'words-count', reflect: false, type: Number})
    public wordsCount = 70;

    @property({attribute: 'chars-count', reflect: false, type: Number})
    public charsCount: number|null = null;

    @property({attribute: 'is-inline', reflect: false, type: Boolean})
    public isInline = false;

    get _readMore() {
      return this.renderRoot?.querySelectorAll(`.${this.blockClassName}`) ?? null;
    }

    connectedCallback() {
        const template = this.querySelector('template');
        this.innerHTML = template?.innerHTML ?? this.innerHTML;

        if (this.children.length > 0) {
            ReadSmore([this], this._options).init()
        }
    }

    get _options(): Options {
        return {
            blockClassName: this.blockClassName,
            moreText: this.moreText,
            lessText: this.lessText,
            wordsCount: this.wordsCount + 1,
            charsCount: this.charsCount,
            isInline: this.isInline,
        }
    }
}
