const glob = require('glob');
const Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

glob.sync('./src/**/*.ts').forEach((file) => {
    let name = file.replace(/\.\/src|\.ts/gi, '');
    Encore.addEntry(name, file);
})

Encore
    .setOutputPath('lib/')
    .setPublicPath('/lib')
    .setManifestKeyPrefix('')
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableTypeScriptLoader()
    .configureTerserPlugin((options) => {
        options.extractComments = false
        options.terserOptions = {
            output: {
                comments: false
            }
        }
    })
;

module.exports = Encore.getWebpackConfig();
